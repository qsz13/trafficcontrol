from cocos.actions import MoveBy

__author__ = 'danielqiu'


class CustomizeMoveBy(MoveBy):
    is_paused = False

    def update(self, t, **kwargs):

        self.target.position = self.start_position + self.delta * t
        car = self.target
        if car.from_direction is "north":
            if car.position[1] < 800:
                car.set_has_started()

        elif car.from_direction is "south":
            if car.position[1] > 0:
                car.set_has_started()

        elif car.from_direction is "west":
            if car.position[0] > 0:
                car.set_has_started()
        elif car.from_direction is "east":
            if car.position[0] < 800:
                car.set_has_started()
